﻿using System;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = true)]
public class SignletonSetting : Attribute {
	public readonly bool Persistent;

	public SignletonSetting() {
		Persistent = false;
	}
	public SignletonSetting(bool isPersistent) {
		Persistent = isPersistent;
	}
}