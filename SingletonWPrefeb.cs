﻿/*
 * File: SingletonWPrefeb.cs
 * =========================
 * 
 * Version				: 1.2
 * Author				: Kleber Lopes da Silva (@kleber_swf / www.linkedin.com/in/kleberswf)
 * E-Mail				: Not Available
 * Copyright			: Not Available
 * Company				: Not Available
 * Script Location		: Plugins/INICPlugins/Other
 * 
 * 
 * Created By			: Kleber Lopes da Silva
 * Created Date			: 2014.11.24
 * 
 * Last Modified by		: Ankur Ranpariya on 2014.11.28 (ankur30884@gmail.com / @PA_HeartBeat)
 * Last Modified		: 2014.11.28
 * 
 * Contributors 		:
 * Curtosey By			: Kleber Lopes da Silva (http://kleber-swf.com/singleton-monobehaviour-unity-projects/)
 * 
 * Purpose
 * ====================================================================================================================
 * 
 * 
 * 
 * 
 * ====================================================================================================================
 * LICENCE / WARRENTY
 * ==================
 * Copyright (c) 2014 <<Developer Name / Company name>> 
 * Please direct any bugs/comments/suggestions to <<support email id>>
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * 
 * Change Log
 * ====================================================================================================================
 * v1.0
 * ====
 * 1. Initial version by "Kleber Lopes da Silva"
 * 
 * v1.1
 * ====
 * 1.	Change DestroyImmediate to Destroy for removing Extra copies of singleton instance, DestroyImmediate wiil remove
 * 		copy imeediately and effect in same frame. It should cause null excption error if destroyed copy is referanced.
 * 		whether Destroy will remove instance end of the frame so it will not cause Null exception in same frame, and next
 * 		frame can identify used referance are removed or not.
 * 2.	Now it will throw Exception insted of logging error.
 * 
 * v1.2
 * ====
 * 1.	Commented if block after Instantiate command.
 * ====================================================================================================================
*/


using System;
using UnityEngine;

/// <summary>
/// <para version="1.1.0.0" />	 
/// <para author="Kleber Lopes da Silva, Ranpariya Ankur" />
/// <para support="" />
/// <para>
/// Description: 
/// </para>
/// </summary>
public abstract class SingletonWPrefeb<T> : MonoBehaviour where T : MonoBehaviour {
	private static T _instance;
	private static bool _instantiated;

	public static T Me {
		get {
			if(_instantiated) {
				return _instance;
			}

			var type = typeof(T);
			var objects = FindObjectsOfType<T>();

			if(objects.Length > 0) {
				Me = objects[0];
				if(objects.Length > 1) {
					Debug.LogWarning("There is more than one instance of Singleton of type \"" + type +
					"\". Keeping the first named" + objects[0].name + ". Destroying the others.");
					for(var i = 1; i < objects.Length; i++) {
						Destroy(objects[i].gameObject);
					}
				}
				_instantiated = true;
			} else {

				var attribute = Attribute.GetCustomAttribute(type, typeof(PrefabInfo)) as PrefabInfo;
				if(attribute == null) {
					throw new Exception("There is no Prefab Atrribute for Singleton of type \"" + type + "\".");
					//Debug.LogError("There is no Prefab Atrribute for Singleton of type \"" + type + "\".");
					//return null;
				}
				var prefabName = attribute.Name;
				if(String.IsNullOrEmpty(prefabName)) {
					throw new Exception("Prefab name is empty for Singleton of type \"" + type + "\".");
					//Debug.LogError("Prefab name is empty for Singleton of type \"" + type + "\".");
					//return null;
				}


				var gameObject = Instantiate(Resources.Load(prefabName)) as GameObject;
				/*
				 // Not require this code, in Unity 4.6.0 if there are no prefab exsits in resouces folder 
				 // Unity will fire "ArgumentException"

				if(gameObject == null) {
					throw new Exception("Could not find Prefab \"" + prefabName + "\" on Resources for Singleton of type \"" + type + "\".",
						new NullReferenceException(" Not able to find Prefab \"" + prefabName + "\"."));
					//Debug.LogError("Could not find Prefab \"" + prefabName + "\" on Resources for Singleton of type \"" + type + "\".");
					//return null;
				}
				*/


				gameObject.name = prefabName;
				Me = gameObject.GetComponent<T>();
				if(!_instantiated) {
					Debug.LogWarning("There wasn't a component of type \"" + type + "\" inside prefab \"" + prefabName + "\". Creating one.");
					Me = gameObject.AddComponent<T>();
				}
				if(attribute.Persistent) {
					DontDestroyOnLoad(_instance.gameObject);
				}
			}
			return _instance;
		}

		private set {
			_instance = value;
			_instantiated = value != null;
		}
	}

	private void OnDestroy() {
		_instantiated = false;
	}
}

[AttributeUsage(AttributeTargets.Class, Inherited = true)]
public class PrefabInfo : Attribute {
	public readonly string Name;
	public readonly bool Persistent;

	public PrefabInfo(string name, bool persistent) {
		Name = name;
		Persistent = persistent;
	}

	public PrefabInfo(string name) {
		Name = name;
		Persistent = false;
	}
}